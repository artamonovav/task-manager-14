package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
